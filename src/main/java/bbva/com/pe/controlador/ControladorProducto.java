package bbva.com.pe.controlador;

import bbva.com.pe.configuraciones.RutasApi;
import bbva.com.pe.modelo.Producto;
import bbva.com.pe.servicio.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(RutasApi.BASE)
public class ControladorProducto {

    @Autowired
    ProductoService productoService;

    @GetMapping(RutasApi.PRODUCTOS)
    public Page<Producto> obtenerProductos(@RequestParam Integer pagina, @RequestParam Integer tamanio) {
        if (pagina == null || pagina < 0) {
            pagina = 0;
        } if (tamanio > 10) {
            tamanio = 10;
        }
        return this.productoService.obtenerProductos(pagina,tamanio);
    }

    @PostMapping(RutasApi.PRODUCTOS_ADD)
    public void agregarProductos(@RequestBody Producto producto) {
        this.productoService.agregarProducto(producto);
    }

    @PutMapping(RutasApi.PRODUCTOS_UPDATE)
    public void actualizarProducto(@PathVariable String codigo, @RequestBody Producto producto) {
        this.productoService.updateProducto(codigo, producto);
    }

    // DELETE
    @DeleteMapping(RutasApi.PRODUCTOS_UPDATE)
    public void eliminarProducto(@PathVariable String codigo) {
        this.productoService.deleteProducto(codigo);
    }

}
