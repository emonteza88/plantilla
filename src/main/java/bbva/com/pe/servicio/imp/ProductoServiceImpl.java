package bbva.com.pe.servicio.imp;

import bbva.com.pe.modelo.Producto;
import bbva.com.pe.repositorio.RepositorioProducto;
import bbva.com.pe.servicio.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class ProductoServiceImpl implements ProductoService {

    @Autowired
    RepositorioProducto repositorioProducto;

    @Override
    public Page<Producto> obtenerProductos(int pagina, int tamanio) {
        return this.repositorioProducto.findAll(PageRequest.of(pagina, tamanio));
    }

    @Override
    public void agregarProducto(Producto producto) {
        this.repositorioProducto.insert(producto);
    }

    @Override
    public void updateProducto(String codigo, Producto producto) {
        producto.setCodigo(codigo);
        this.repositorioProducto.save(producto);
    }

    @Override
    public void deleteProducto(String codigo) {
        this.repositorioProducto.deleteById(codigo);
    }
}
